# Monitoring Internet Archive backups of domains #

This php script is intended to monitor websites that have been archived on archive.org and log when they change. 

Disclaimer: this is a rather hastily written and sparsely tested script that I am using for a specific task, and am making available with no guarantees in hopes that others will find it useful.

### What It Does ###

First, the script downloads a list of urls to crawl from the internet archive repository specified. This is the only interaction with archive.org.
For each URL, the script initiates a GET request via CURL. After downloading the response header and the first 1kb of the response body from the specified URL, the CURL request aborts and the rest of the response is not downloaded. 
Information from the response is saved locally in a directory called ".changecrawler" created in the current directory when you run the script. For each file, a hash of the first 1kb of the responsy body is saved, as well as the Content-Length and Date-Modified headers if they are present. 
The script continues to loop through the urls repeatedly. When a response hash has already been recorded, it compares the current field from the last recorded field. If a change has occurred in either header field or the body hash, details about the change are logged in a local log file.

### Prerequisites ####

* PHP - The script is tested with PHP v5.6.25.
* Python
* [IA command line tools](https://pypi.python.org/pypi/internetarchive)
* You must have a domain that has already been fully archived up in the [Internet Archive](http://www.archive.org) in the format expected by the script. 

Specifically, I am expecting that there is a resource in the internet archive whose identifier is the domain name that you have backed up, and that the command `ia list www.YOUR_DOMAIN.org` outputs a list of all relative urls that you intend to monitor. 

This assumes that you (or someone else) has downloaded an entire domain using a command such as:
~~~~
wget --random-wait --mirror -e robots=off -r -p -U Mozilla http://www.*YOUR_DOMAIN*.org
~~~~
and then uploaded it to the internet archive with a command such as:
~~~~
 ia -l upload -cv www.*YOUR_DOMAIN*.org www.*YOUR_DOMAIN*.org --retries 10 --delete
~~~~
There are a lot of sites already up on the archive that have been backup up this way, that you can help to monitor for changes. You could test it, for example, on the government's child welfare site www.childwelfare.gov, which has been backed up on archive.org in the method described above.


### Installation ###
~~~
git clone https://ethenry@bitbucket.org/ethenry/ia_speedy_change_monitoring.git
cd ia_speedy_change_monitoring
~~~

### Usage ###
To run the script, use the following command:
~~~
./ia_monitor.php www.*YOUR_DOMAIN*.org *YOUR_LOGFILE*.log
~~~
this will loop forever checking each url for changes and logging any changes it finds. You can run it in the background with a command like 
~~~
./ia_monitor.php www.*YOUR_DOMAIN*.org *YOUR_LOGFILE*.log &
~~~
If anyone wants to turn this into a service, that would be a very nice addition.

### Contribution guidelines ###
Everyone interested is welcome to contribute. Don't be shy to get in touch if you have any questions or suggestions.