#!/usr/bin/php
<?php
$limit =  1000;
$bodydata = "";
$headerdata = "";
$domain = $argv[1];
$logfilename = isset($argv[2]) ? $argv[2] : "./ia_monitoring.log";
touch($logfilename);
error_reporting(E_ALL & E_DEPRECATED & E_NOTICE);

$hashdir = './.changecrawler/';
if (!file_exists($hashdir)) 
    mkdir($hashdir, 0777, true);

exec("ia list $domain",$urls);

$loop = 0;
while(true) {
    $loop++;
    echo "\nloop number $loop\n\n";
    foreach ($urls as $url) {
        $url = rtrim($domain."/".$url);    
        echo "processing: ".$url."\n";

        #this function sets the 'headerdata' and 'bodydata' globals
        # as a side effect
        $response = special_get($url);   
        $headers = parse_headers($headerdata);
        $md5 = md5(substr($bodydata,0,$limit));
        $oldheaders = false;
        $oldhash = false;
        $hashfilename = $hashdir.$url;

        $pathinfo = pathinfo($hashfilename);
        if (!file_exists($pathinfo['dirname'])) {
            mkdir($pathinfo['dirname'], 0777, true);
        }
        if(file_exists($hashfilename)) {
            $oldheaders = [];
            $last_check = filemtime($hashfilename);
            $hashfile = fopen($hashfilename, "r");
            $oldhash = rtrim(fgets($hashfile));
            while (($line = fgets($hashfile)) !== false) {

                $split = explode(": ",rtrim($line));
                $oldheaders[$split[0]] = $split[1];
            }
            fclose($hashfile);
        }

        $hashfile = fopen($hashfilename, "w");
        fwrite($hashfile,$md5.PHP_EOL);
        if($oldhash && $md5 !== $oldhash)
            log_change($url,'hash',$headers['content_type'],$oldhash,$md5,$last_check);
        check_header('content-length',$hashfile,$headers,$oldheaders,$url);
        check_header('date-modified',$hashfile,$headers,$oldheaders,$url);
        fclose($hashfile);
    }
}

function check_header($header_name,$hashfile,$headers,$oldheaders,$url) {
    if(isset($headers[$header_name])) {
        fwrite($hashfile,"$header_name: ".$headers[$header_name].PHP_EOL);
        if($oldheaders && isset($oldheaders[$header_name]) && $headers[$header_name] && $oldheaders[$header_name] !==  $headers[$header_name]) 
            log_change($url,$header_name,$headers[$header_name],$oldheaders[$header_name],$headers[$header_name],$last_check);
    }
}


function special_get($url) {
    global $bodydata;
    global $headerdata;
    global $limit;
    $bodydata = "";
    $headerdata = "";

    $writefn = function($ch, $chunk) { 
        global $bodydata;
        global $limit;
        $len = strlen($bodydata) + strlen($chunk);
        if ($len >= $limit ) {
            $bodydata .= substr($chunk, 0, $limit+500);
            return -1;
        }
        $bodydata .= $chunk;
        return strlen($chunk);
    };

    $headerfn = function($ch, $chunk) { 
        global $headerdata;
        global $limit;
        $headerdata .= $chunk;
        return strlen($chunk);
    };

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/4.0');
    curl_setopt($ch, CURLOPT_BINARYTRANSFER, 1);
    curl_setopt($ch, CURLOPT_WRITEFUNCTION, $writefn);
    curl_setopt($ch, CURLOPT_HEADERFUNCTION, $headerfn);
    $response = curl_exec($ch);
    curl_close($ch); 
    $bodydata = substr($bodydata, 1000);
};

function log_change($url,$property,$content_type,$oldvalue,$newvalue,$last_check){
    global $domain;
    $last_check = isset($last_check) ? $last_check : "";
    $logfile = fopen($logfilename,'a');
    fwrite($logfile,"\nurl: $url\nproperty: $property\ncontent type: $content_type\nold value: $oldvalue\nnew value: $newvalue\ntime detected: ".date('Y-m-d H:i:s')."\nlast check: ".date('Y-m-d H:i:s',$last_check)."\n-----------\n\n");
    fclose($logfile);    
}

function parse_headers($headerstring)
{
    $headers = http_parse_headers ($headerstring);
    $date_modified = "";
    $content_length = "";
    foreach($headers as $header_name => $header_value) {
        switch(strtolower($header_name)) {
            case "date-modified":
                $date_modified = $header_value;
                break;
            case "content-type":
                $content_type = $header_value;
                break;
            case "content-length":
                $content_length = $header_value;
                break;
        }
    }
    return ["content-length" => $content_length,
            "date-modified" => $date_modified,
            "content-type" => $content_type];
}

function http_parse_headers( $header )
{
    $retVal = array();
    $fields = explode("\r\n", preg_replace('/\x0D\x0A[\x09\x20]+/', ' ', $header));
    foreach( $fields as $field ) {
        if( preg_match('/([^:]+): (.+)/m', $field, $match) ) {
            $match[1] = preg_replace('/(?<=^|[\x09\x20\x2D])./e', 'strtoupper("\0")', strtolower(trim($match[1])));
            if( isset($retVal[$match[1]]) ) {
                $retVal[$match[1]] = array($retVal[$match[1]], $match[2]);
            } else {
                $retVal[$match[1]] = trim($match[2]);
            }
        }
    }
    return $retVal;
}

?>
